# coding: utf-8
import requests
import json
import os
import sys
import boto3
import datetime
import base64

from config import Config
import sfdc_helper
import pysalesforce
import webservice

def lambda_handler(event, context):

    result = {}
    result['success'] = []
    result['failed'] = []

    if 'body' in event:
        try:
            event = json.loads(event['body'])
        except Exception as e:
            result['failed'].append('Error with request body, contact your administrator')
            return response(result)
    print(event)
    if(event['relocation']['update_relocation']):
        field_validate_pass, field_validate_message = update_request_validation(event)
        if not field_validate_pass:
            result['failed'].append(field_validate_message)
            return response(result)
    else:
        field_validate_pass, field_validate_message = create_request_validation(event)
        if not field_validate_pass:
            result.get('failed').append(field_validate_message)
            return response(result)

    print('logging into Salesforce')
    auth = sfdc_authorization()

    if(not(event['relocation']['update_relocation'])):
        event = primaryReasonMapping(event);

    if 'instance_url' in auth:
        url = auth['instance_url'] + '/services/apexrest/submitRelocation'
        payload = json.dumps(event)
        headers = {
            'content-type': "application/json",
            'Authorization': 'OAuth ' + auth['access_token'],
            'cache-control': "no-cache"
        }
        print('sending request to sf: {} '.format(payload))
        res = requests.request("POST", url, data=payload, headers=headers)
        print ('response from sf: {} '.format(res.text))
        return response(json.loads(res.text))
    else:
        result['failed'].append('Unable to connect to Salesforce')
        return response(result)

def create_request_validation(event):
    validation_pass = True
    validation_message = ''
    missing_fields = []

    data_object_case = event['relocation']['reloCase']

    if data_object_case.get('eid') is None or not(data_object_case.get('eid')):
        missing_fields.append('eid')

    if data_object_case.get('language') is None or not(data_object_case.get('language')):
        missing_fields.append('language')

    if data_object_case.get('reason') is None or not(data_object_case.get('reason')):
        missing_fields.append('reason')

    data_object_guests = event['relocation']['guests']
    for item in data_object_guests:
        if item.get('guestResolution') is None or not(item.get('guestResolution')):
            missing_fields.append('guestResolution')

        if item.get('bookingId') is None or not(item.get('bookingId')):
            missing_fields.append('bookingId')

    if len(missing_fields) > 0:
        validation_message = 'One or more required fields are missing: '
        validation_message += ",".join(missing_fields)
        validation_pass = False
    return validation_pass, validation_message

def update_request_validation(event):
    validation_pass = True
    validation_message = ''
    missing_fields = []

    data_object_guests = event['relocation']['items']
    for item in data_object_guests:
        if item.get('bookingId') is None or not(item.get('bookingId')):
            missing_fields.append('bookingId')

        if item.get('relocatedEID') is None or not(item.get('relocatedEID')):
            missing_fields.append('relocatedEID')

        if len(missing_fields) > 0:
            validation_message = 'One or more required fields are missing: '
            validation_message += ",".join(missing_fields)
            validation_pass = False
    return validation_pass, validation_message

def response(message):
    if 'failed' in message and not message['failed']:
        statuscode = 200
    else:
        statuscode = 400
    print ('response: {}, {} '.format(message, statuscode))
    return {"isBase64Encoded": False, "statusCode": statuscode, "body": message }

def sfdc_authorization():
    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    sf_config_id = os.environ["sf_config"]


    sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
    sfdc_access_token = sfdc_login_response.get('access_token')
    sfdc_instance_url = sfdc_login_response.get('instance_url')
    '''
    payload = {
        'grant_type': 'password',
        'client_id': '3MVG9GiqKapCZBwGSTPX2Wf.wQmWw8zed5ZcL4sDifiEjee2hsKXf0u4T2cB2nHaOXLxutK3zcax4xpQZ4iGt',
        'client_secret': '8665074494016993811',
        'username': 'tvenkatesh@expedia.com.brucewayne',
        'password': 'Expedia@'
    }
    oauth_url = 'https://test.salesforce.com/services/oauth2/token'
    r = requests.post(oauth_url,
                      headers={'Content-Type': 'application/x-www-form-urlencoded'},
                      data=payload)
    body = json.loads(r.text)'''
    return sfdc_login_response

def primaryReasonMapping(event):
    reason = event['relocation']['reloCase'].get('reason')
    if  reason == 'Not able to honor amenity booked':
        event['relocation']['reloCase']['reason'] = 'Amenity Unavailable'


    elif  reason == 'Act of nature or governmental requirement':
        event['relocation']['reloCase']['reason'] = 'Force Majeure'


    elif  reason == 'Fraudulent Activity':
        event['relocation']['reloCase']['reason'] = 'Fraudulent Activity'


    elif  reason == 'Temporarily Closed':
        event['relocation']['reloCase']['reason'] = 'Seasonal Hotel Closure'


    elif  reason == 'Property Overbooked':
        event['relocation']['reloCase']['reason'] = 'Hotel Overbooked - Inv not Closed'


    elif  reason == 'Unable to honor accessible/handicap option':
        event['relocation']['reloCase']['reason'] = 'Accessibility Not Available'


    elif  reason == 'Room or property is a health or safety risk':
        event['relocation']['reloCase']['reason'] = 'Health and Safety'


    elif  reason == 'Content was displayed incorrectly on website':
        event['relocation']['reloCase']['reason'] = 'Content Issue'


    elif reason == 'Rate booked is too low':
        event['relocation']['reloCase']['reason'] = 'Rate Refused by Hotel'


    elif  reason == 'Guest is banned from property':
        event['relocation']['reloCase']['reason'] = 'Blacklisted Guest'


    elif reason == 'Expedia Virtual Card will not authorize':
        event['relocation']['reloCase']['reason'] = 'EVC Declined'


    elif  reason == 'Permanently Closed':
        event['relocation']['reloCase']['reason'] = 'Permanent Hotel Closure'


    elif  reason == 'Planned Maintenance or Remodeling':
        event['relocation']['reloCase']['reason'] = 'Planned Maintenance'


    elif  reason == 'Unexpected Maintenance':
        event['relocation']['reloCase']['reason'] = 'Unplanned Issue'


    elif  reason == 'Will not be open for advertised check-in hours':
        event['relocation']['reloCase']['reason'] = 'Closed for Advertised Check-In'

    return event

if __name__ == '__main__':
    jsonData ={ 
        "relocation":{
              "update_relocation": False,
              "reloCase":{
                  "eid":"77",
                  "language":"en",
                  "totalGuests":1,
                  "reason":"Overbooked",
                  "subReason":"Inventory was not closed",
                  "contactEmail":"tvmchow@gmail.com",
                  "contactFirstName":"TVM",
                  "contactLastName":"Chow",
                  "serviceID":"e12735cd-f109-4887-accd-392ccde75367",
                  "contactTitle":"",
                  "ipAddress":"4444"
              },
              "guests":[{
                  "guestResolution":"Request Expedia relocation",
                  "supplierResponse":"Hotel Accepted",
                  "check_in":"2018-9-9",
                  "check_out":"2018-9-9",
                  "suggestedHotels":["12345","123457","12456"],
                  "quotedRelocationPrice":"4352.02",
                  "bookingId":"426860832",
                  "ex_rate_usdtosupplier":24.234,
                  "customerPhone":"34092529398",
                  "customerCountryCode":"+1",
                  "TPID":"1",
                  "EAPID":"1",
                  "affiliate":{
                      "affiliateConfirmationId":"",
                      "affiliateName":"",
                      "affiliateID":"",
                      "affiliate_Escalation_Phone_Number":"",
                      "affiliate_Escalation_Email_Address":""
                 }
             },
                  {
                      "guestResolution": "Walsk",
                      "supplierResponse": "Hotel Accepted",
                      "check_in": "2018-9-9",
                      "check_out": "2018-9-9",
                      "suggestedHotels": ["12345", "123457", "12456"],
                      "quotedRelocationPrice": "4352.02",
                      "bookingId": "426860832",
                      "ex_rate_usdtosupplier": 24.234,
                      "customerPhone": "34092529398",
                      "customerCountryCode": "+1",
                      "TPID": "1",
                      "EAPID": "1",
                      "affiliate": {
                          "affiliateConfirmationId": "",
                          "affiliateName": "",
                          "affiliateID": "",
                          "affiliate_Escalation_Phone_Number": "",
                          "affiliate_Escalation_Email_Address": ""
                      }
                  }
              ]
         }
      }
    updatejsonData = {"relocation":{
        "update_relocation" : True,
        "items":[
            {
                    "caseNumber":"63517086",
                    "bookingId":"637304325",
                    "selectedQuotedRelocationPrice":"1122.02",
                    "relocatedEID":"10995",
                    "supplierResponse":"Hotel did not respond",
                    "ex_rate_usdtosupplier":22.34
            }
        ]
        }
    }
    lambda_handler(jsonData,'')

